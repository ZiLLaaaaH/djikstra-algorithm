// The Dijkstra's algorithm
STDMETHODIMP CDijkstra::ShortestPath(long node1, long node2)
{
    ReleaseGraph();
    // init d and pi
    InitializeSource(g, g.m_nodes[node1-1]);
    // Set S empty
    VTYPE_NODE S;
    // Put nodes in Q
    VTYPE_NODE Q;
    VTYPE_NODE::iterator kl;
    for(kl=g.m_nodes.begin(); kl<g.m_nodes.end(); kl++)
    {
        CNode node = (*kl).Copy();
        Q.push_back(node);
    }
    // Algorithm
    while(Q.size())
    {
        CNode nod = ExtractMin(Q); // the minim value for 
              // the shortest path up to this step
        S.push_back(nod);
        // each vertex v which is a neighbour of u
        VTYPE_NODE::iterator kl;
        for(kl=g.m_nodes.begin(); kl<g.m_nodes.end(); kl++)
        {
            if(ExistEdge(nod, (*kl)))
            {
                bool gasit = false;
                VTYPE_NODE::iterator kll;
                for(kll=Q.begin(); kll<Q.end(); kll++)
                {
                    if((*kll).m_NodeNr == (*kl).m_NodeNr)
                    gasit = true;
                }
                if(gasit)
                    Relax(nod, (*kl), GetEdgeVal(nod, (*kl)));
            }
        }
    }
    RefreshDone(node1, node2);
    return S_OK;
}